﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    //All Variables
    public LayerMask tankMask;
    public ParticleSystem explosionParticles;
    public AudioSource explosionAudio;
    public float maxDamage = 100f;
    public float explosionForce = 1000f;
    public float lifeTime = 2f;
    public float explosionRadius = 5f;

    // Start is called before the first frame update
    void Start()
    {
        //Destroy the gameObject during the time you put
        Destroy(gameObject, lifeTime);
    }

    //When it triggers gets collider, damage, rigidbody, and makes the shell explode
     void OnTriggerEnter(Collider col)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, tankMask);
        for(int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();
            if(!targetRigidbody)
            
                continue;
                targetRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);
                TankHp targetHealth = targetRigidbody.GetComponent<TankHp>();
                if(!targetHealth)
   
                    continue;
                    float damage = Damage(targetRigidbody.position);
                    targetHealth.TakeDamage(damage);
                
            
        }

        explosionParticles.transform.parent = null;
        explosionParticles.Play();
        explosionAudio.Play();
        Destroy(explosionParticles.gameObject, explosionParticles.main.duration);
        Destroy(gameObject);
    }

    //The Damage output of our shell which makes our tank fall back on explotion radius
    public float Damage(Vector3 targetPosition)
    {
        Vector3 explosionToTarget = targetPosition - transform.position;
        float explosionDistance = explosionToTarget.magnitude;
        float relativeDistance = (explosionRadius - explosionDistance) / explosionRadius;
        float damage = relativeDistance * maxDamage;
        damage = Mathf.Max(0f, damage);
        return damage;
    }
}
