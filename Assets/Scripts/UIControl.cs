﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControl : MonoBehaviour
{
    //All Variables
    public bool useRotation = true;
    private Quaternion Rotation;

    void Start()
    {
        //Rotation for the UI
        Rotation = transform.parent.localRotation;
    }

    //While we Rotate Ui will too
    void Update()
    {
        if(useRotation)
        {
            transform.rotation = Rotation;
        }
    }    
}
