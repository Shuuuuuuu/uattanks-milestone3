﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemManager : MonoBehaviour
{
    //Variables used
    public GameObject addHealth;
    public GameObject addDamage;
    public GameObject addBarrel;
    public int ItemType = 0;
    public GameObject[] itemPos;

    
    public int noItem = 0;
    System.DateTime curTime = new System.DateTime();
    public int item = 0;
    public int Itempos = 0;
    public int randItem = 0;
    public int randItemPos = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Invoke the items and repeat
        InvokeRepeating("CreateItem", 5, 20);
    }

    //If noitem is greater than 0 return no item
    int NoItemControl()
    {
        if(noItem > 0)
        {
            noItem--;
        }
        return noItem;
    }

    //Creating the item with a random variable and Postions
    void CreateItem()
    {
       
            System.Random rand = new System.Random();
            do
            {
                randItem = rand.Next(0, 3);
            }

            while (item == randItem);
            item = randItem;

            do
            {
                randItemPos = rand.Next(0, 6);
            }

            while (Itempos == randItemPos);
            Itempos = randItemPos;

        if (noItem < 2)
        {

            //Use Switch statement to instantiate the items randomly
            switch (item)
            {
                case 0:
                    Instantiate(addHealth, itemPos[Itempos].transform.position, itemPos[Itempos].transform.rotation);
                    noItem++;
                    break;

                case 1:
                    Instantiate(addDamage, itemPos[Itempos].transform.position, itemPos[Itempos].transform.rotation);
                    noItem++;
                    break;

                case 2:
                    Instantiate(addBarrel, itemPos[Itempos].transform.position, itemPos[Itempos].transform.rotation);
                    noItem++;
                    break;
            }
        }
    }

    //When trigger with player GameObject dissapears
    public void OnTriggerEnter(Collider collider)
    {
        GameObject.Destroy(this.gameObject);

        if(collider.tag == "Tank")
        {
            if(ItemType == 0)
            {
                collider.SendMessage("addHealth");
                GameObject.Find("ItemManager").SendMessage("NoItemControl");
            }

            if(ItemType == 1)
            {
                collider.SendMessage("addDamage");
                GameObject.Find("ItemManager").SendMessage("NoItemControl");
            }

            if(ItemType == 2)
            {
                collider.SendMessage("addSpeed");
                GameObject.Find("ItemManager").SendMessage("NoItemControl");
            }
        }
    }
}
