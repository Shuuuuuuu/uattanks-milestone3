﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl1 : MonoBehaviour
{
    //Variables
    public Transform playerPosition;
    private Vector3 offset;

    void Start()
    {
        //Position of the camera getting the player
        offset = playerPosition.position - transform.position;
    }

    void Update()
    {
        //if there is no player position go off
        if(playerPosition == null)
        {
            return;
        }
        transform.position = playerPosition.position - offset;
    }
}
