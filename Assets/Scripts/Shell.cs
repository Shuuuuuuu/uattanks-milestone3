﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    //All Variables
    public GameObject shellExplosionPrefab;
    public AudioClip shellExplosionAudio;

    //Trigger Collider, when it collides with the player or enemy it will send a message to damage the GameObject
    public void OnTriggerEnter(Collider collider)
    {
        AudioSource.PlayClipAtPoint(shellExplosionAudio, transform.position);
        GameObject.Instantiate(shellExplosionPrefab, transform.position, transform.rotation);
        GameObject.Destroy(this.gameObject);
        if(collider.tag == "Player" || collider.tag == "Enemy")
        {
            collider.SendMessage("TakeDamage");
        }
    }
}
