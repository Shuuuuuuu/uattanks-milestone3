﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankHp : MonoBehaviour
{
    //All Variables
    public float maxHealth = 100f;
    public Slider slider;
    public Image fill;
    public Color healthColor = Color.green;
    public Color zeroColor = Color.red;
    public GameObject DeathExplosionPrefab;
    private AudioSource explosion;
    private ParticleSystem explosionParticles;
    public float curHealth;
    private bool dead;

    void Awake()
    {
        //Call in the explosion particles when gameObject is needed
        explosionParticles = Instantiate(DeathExplosionPrefab).GetComponent<ParticleSystem>();
        explosion = explosionParticles.GetComponent<AudioSource>();
        explosionParticles.gameObject.SetActive(false);
    }
    
    //OnEnable is our tank current health and set our UI to go with our hp
    void OnEnable()
    {
        curHealth = maxHealth;
        dead = false;
        SetHealthUI();
    }

    //What will happen to our tank when it is damage, Ui will follow and if it reaches 0 Call the Death Function
   public void TakeDamage(float amount)
    {
        curHealth -= amount;
        SetHealthUI();
        if(curHealth <= 0f && !dead)
        {
            Death();
        }
    }
    
    //Call in our health slider and fill colors
    void SetHealthUI()
    {
        slider.value = curHealth;
        fill.color = Color.Lerp(zeroColor, healthColor, curHealth / maxHealth);
    }

    //What happends when our tank dies explodes and plays audio
    void Death()
    {
        dead = true;
        explosionParticles.transform.position = transform.position;
        explosionParticles.gameObject.SetActive(true);
        explosionParticles.Play();
        explosion.Play();
        gameObject.SetActive(false);
    }

    //When touch the AddHealth item gain health and UI will follow
    float addHealth()
    {
        curHealth += 5;
        SetHealthUI();
        return curHealth;
    }
}
