﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //All Variables Used
    public int numRounds;
    public float startDelay;
    public float endDelay;
    public CameraControl cameraControl;
    public Text messageText;
    public GameObject[] tankPrefab;
    public TankManager[] Tanks;

    private int roundNum;
    private WaitForSeconds startWait;
    private WaitForSeconds endWait;
    private TankManager roundWinner;
    private TankManager gameWinner;

    private void Start()
    {
        //Create delays to be made just once
        startWait = new WaitForSeconds(startDelay);
        endWait = new WaitForSeconds(endDelay);
        SpawnAllTanks();
        SetCameraTargets();
        StartCoroutine(GameLoop());
    }

    private void SpawnAllTanks()
    {
        //Creates the Tanks and set their player number and references for control
        for (int i = 0; i < Tanks.Length; i++)
        {
            Tanks[i].Instance = 
                Instantiate(tankPrefab[i], Tanks[i].spawnPoint.position, Tanks[i].spawnPoint.rotation) as GameObject;
            Tanks[i].playerNumber = i + 1;
            Tanks[i].Setup();
        }
    }

    private void SetCameraTargets()
    {
        //Gets the target to follow around
        Transform[] targets = new Transform[Tanks.Length];
        for(int i = 0; i < targets.Length; i++)
        {
            targets[i] = Tanks[i].Instance.transform;
        }
        cameraControl.Targets = targets;
    }

    //IEnumeratior to generate a Loop for when the Rounds Start, Plays and Ends
    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStart());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnd());
        if(gameWinner != null)
        {
            SceneManager.LoadScene("Menu");
        }

        else
        {
            StartCoroutine(GameLoop());
        }
    }

    //Everything that happends when roundstarts 
    private IEnumerator RoundStart()
    {
        Reset();
        DisableControl();
        cameraControl.StartPositionAndSize();
        roundNum++;
        messageText.text = "Round: " + roundNum;
        yield return startWait;
    }

    //Everything that happens when round is playing
    private IEnumerator RoundPlaying()
    {
        EnableControl();
        messageText.text = "";
        while (!OneTankLeft())
        {
            yield return null;
        }
    }

    //Everything that happends when round ends
    private IEnumerator RoundEnd()
    {
        DisableControl();
        roundWinner = null;
        roundWinner = GetWinner();
        if (roundWinner != null)
            roundWinner.win++;
        gameWinner = GetGameWinner();
        string message = EndMessage();
        messageText.text = message;
        yield return endWait;
    }

    //How the TankManager gets the round Winner
    private TankManager GetWinner()
    {
        for (int i = 0; i < Tanks.Length; i++)
        {
            if (Tanks[i].Instance.activeSelf)
                return Tanks[i];
        }

        return null;
    }

    //How the TankManager gets the Game Winner
    private TankManager GetGameWinner()
    {
        for(int i = 0; i < Tanks.Length; i++)
        {
            if (Tanks[i].win == numRounds)
                return Tanks[i];
        }

        return null;
    }

    //End Message depending on the outcome of the game
    private string EndMessage()
    {
        string message = "Draw";
        if (roundWinner != null)
            message = roundWinner.coloredPlayerText + " Wins the round!";
        message += "\n\n\n\n";
        for(int i = 0; i < Tanks.Length; i++)
        {
            message += Tanks[i].coloredPlayerText + ": " + Tanks[i].win + " Wins\n";
        }

        if (gameWinner != null)
            message = gameWinner.coloredPlayerText + " Wins the game!";
        return message;
    }

    //When one tank is left activate the Message fo the winner
    private bool OneTankLeft()
    {
        int numTanksLeft = 0;
        for(int i = 0; i < Tanks.Length; i++)
        {
            if (Tanks[i].Instance.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }

    //This reset the tanks to their positions
    private void Reset()
    {
        for(int i = 0; i < Tanks.Length; i++)
        {
            Tanks[i].Reset();
        }
    }

    //Enables the control of the tanks after the message
    private void EnableControl()
    {
        for(int i = 0; i < Tanks.Length; i++)
        {
            Tanks[i].EnableControl();
        }
    }

    //Disables the control of tanks during any messages
    private void DisableControl()
    {
        for(int i = 0; i < Tanks.Length; i++)
        {
            Tanks[i].DisableControl();
        }
    }
}
