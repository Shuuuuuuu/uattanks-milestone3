﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    //Variables
    public Text gameOver;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Find PvEPlayer
        GameObject player = GameObject.Find("PvEPlayer");

        //Find ScoreController Object
        GameObject go = GameObject.Find("ScoreController");

        //Get The Score go component, if there is no player it is GameOver press R to go to the Menu
        Score other = go.GetComponent<Score>();
        if(null == player)
        {
            gameOver.text = "GameOver your score is: " + other.score + "\n Please press R to Go To The Menu";
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("Menu");
            }
        }
    }
}
