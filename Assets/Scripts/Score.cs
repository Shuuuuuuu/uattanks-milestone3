﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    //All Variables
    public Text scoreText;
    public int score;

    // Start is called before the first frame update
    void Start()
    {
        //The Score you start and the updater of your score
        score = 0;
        UpdateScore();
    }

    void UpdateScore()
    {
        //Score text that will be available in the screen
        scoreText.text = "Score: " + score;
    }

    //The score adder when you kill and enemy
    public void AddScore(int value)
    {
        score += value;
        UpdateScore();
    }
}
